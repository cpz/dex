describe('app', () => {
  it('connect websockets response', (done) => {
    expect.assertions(1);

    const ws = new WebSocket(`ws://localhost:3000`).on('message', (msg) => {
      expect(JSON.parse(msg).time).toEqual(0);
      ws.close();
    })
    .on('close', () => done());
  });
});
