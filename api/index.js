// var app = require('express')()
var http = require('http').Server()
var io = require('socket.io')(http, {'pingTimeout': 7000, 'pingInterval': 3000});

io.on('connection', socket => {
  console.log(`User connected ${socket.id}`)

  socket.on('message', data => {
    socket.broadcast.emit("message", data)
    console.log('broadcast', data)
  })
})
// console.log('port now', process.env.PORT)
http.listen(3000, () => {
  console.log('Listening on *:3000')
})
