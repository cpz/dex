# Dex-test

## Video sync with websockets

## Project setup
```
yarn install / npm i
```

### local dev backend
```
yarn api
or
npm run api
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Run your unit tests
```
yarn test:unit
```